@extends('template')

@section('contenu')
<div class="container">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">Ajout d'un article</div>
                <div class="panel-body">
                    <form action="{{ route('post.store') }}" method="POST">
                        @csrf
                        <div class="form-group {{ $errors->has('titre') ? 'has-error' : '' }}">
                            <input type="text" name="titre" class="form-control" placeholder="Titre">
                            @if ($errors->has('titre'))
                                <small class="text-danger">{{ $errors->first('titre') }}</small>
                            @endif
                        </div>
                        <div class="form-group {{ $errors->has('contenu') ? 'has-error' : '' }}">
                            <textarea name="contenu" class="form-control" placeholder="Contenu"></textarea>
                            @if ($errors->has('contenu'))
                                <small class="text-danger">{{ $errors->first('contenu') }}</small>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="tags">Mots-clés (séparés par des virgules):</label>
                            <input type="text" class="form-control" id="tags" name="tags" placeholder="Entrez les mots-clés">
                            @error('tags')
                            <small class="text-danger">{{ $message }}</small>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-info pull-right">Envoyer !</button>
                    </form>
                </div>
            </div>
            <a href="{{ url()->previous() }}" class="btn btn-primary">
                <span class="glyphicon glyphicon-circle-arrow-left"></span> Retour
            </a>
        </div>
    </div>
</div>
@endsection
