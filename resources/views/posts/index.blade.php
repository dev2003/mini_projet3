@extends('template')

@section('header')
@auth

    <div class="btn-group pull-right">
        <a href="{{ route('post.create') }}" class="btn btn-info">Créer un article</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: inline;">
            @csrf
            <button type="submit" class="btn btn-warning">Déconnexion</button>
        </form>
    </div>
@endauth

@guest
    <a href="{{ url('login') }}" class="btn btn-info pull-right">Se connecter</a>
    <a href="{{ route('register') }}" class="btn btn-info pull-right">S'inscrire</a>
@endguest
@endsection

@section('contenu')
@if(isset($info))
    <div class="row alert alert-info">{{ $info }}</div>
@endif

<div class="d-flex">
        {{ $posts->links() }}
    </div>


@foreach($posts as $post)
    <article class="row bg-primary">
        <div class="col-md-12">
            <header>
                <h1>{{ $post->titre }}</h1>
            </header>
            <hr>
            <section>
                <p>{{ $post->contenu }}</p>
          
             @auth
                    <form method="POST" action="{{ route('post.destroy', $post->id) }}">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Supprimer cet article</button>
                    </form>
            @endauth
            
                <em class="pull-right">
                    <span class="glyphicon glyphicon-pencil"></span>
                    {{ $post->user->name }} le {{ $post->created_at->format('d-m-Y') }}
                </em>
            </section>
        </div>
    </article>
    <br>
@endforeach
@endsection
