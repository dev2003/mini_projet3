<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true; // Vous pouvez ajouter la logique d'autorisation appropriée ici
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'titre' => 'required|max:80',
            'contenu' => 'required',
            'tags' => ['regex:/^[A-Za-z0-9-éèàù]{1,50}?(,[A-Za-z0-9éèàù]{1,50})*$/']
     
        ];
    }


     
     
    public function messages()
    {
        return [
            'tags.regex' => 'Les mots-clés, séparés par des virgules (sans espaces), doivent avoir au maximum 50 caractères alphanumériques.'
        ];
    }
}