<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 

class AuthController extends Controller
{
    public function show()
    {
        return view('show');
    }

    public function login(Request $request)
    {
        $password=$request->password;
        $email=$request->email;
        $credentials =["email"=>$email ,"password"=>$password];
      

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return to_route('post.index');
        }

        return back()->withErrors([
            'email' => 'Incorrect email or password.',
        ]);
    }
}
