<?php

namespace App\Http\Controllers;


use App\Models\Post;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::latest()->paginate(2);
        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\PostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titre' => 'required|string|max:80',
            'contenu' => 'required',
        ]);

        
        $post = Post::create([
            'titre' => $request->titre,
            'contenu' => $request->contenu,
            'user_id' => auth()->id(),
        ]);

        return redirect()->route('post.index')->with('success', 'Post créé avec succès.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PostRequest  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $post->update($request->validated());

        return redirect()->route('posts.index')->with('success', 'Post updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post , Request $request)
{
    // Vérifiez si l'utilisateur peut supprimer le post
    // if (Request->user()->can('delete', $post)) {
    //     abort(403);
    // }

    // Supprimer le post
    $post->delete();

    return redirect()->back(); 
}



    // public function indexTag($tagUrl)
    // {
      
    //     $tag = Tag::where('tag_url', $tagUrl)->first();

    //     if ($tag) {
         
    //         $posts = Post::whereHas('tags', function ($query) use ($tag) {
    //             $query->where('tag_id', $tag->id);
    //         })->paginate(10);

         
    //         return view('posts.tagged', compact('posts', 'tag'));
    //     } else {
           
    //         return redirect()->route('post.index')->with('error', 'Aucun article trouvé pour ce tag.');
    //     }
    // }




    // <div class="pull-right">
    //                         @foreach($post->tags as $tag)
    //                         <a href="{{ url('post/tag/' . $tag->tag_url) }}" class="btn btn-xs btn-info">{{ $tag->tag }}</a>
    //                         @endforeach
    //                     </div> 
}