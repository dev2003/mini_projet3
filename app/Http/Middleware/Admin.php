<?php


namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class Admin
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->user()->admin) {
            return redirect()->back()->with('error', 'Seuls les personnes n-est pas autorisés
            à effectuer cette action.')->withInput();
            }
        return $next($request);
    
    }
}


