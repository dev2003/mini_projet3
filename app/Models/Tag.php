<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Post; 

class Tag extends Model
{
    protected $table = 'votre_nom_de_table_tags';
    protected $fillable = ['tag', 'tag_url'];

    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }
}

