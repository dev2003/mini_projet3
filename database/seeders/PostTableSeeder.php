<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PostTableSeeder extends Seeder
{
    public function run()
    {
        // DB::table('posts')->delete(); // Remove this line to prevent seeding
    
        for ($i = 0; $i < 100; ++$i) {
            $date = $this->randDate();
    
            DB::table('posts')->insert([
                'title' => 'Titre ' . $i,
                'content' => 'Contenu ' . $i . ' Lorem ipsum dolor sit amet, consectetur adipisicing elit, 
                    sed do eiusmod incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu 
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui',
                'user_id' => 1,
                'created_at' => $date,
                'updated_at' => $date
            ]);
        }
    }
    
    private function randDate()
    {
        return \Carbon\Carbon::now()->subDays(rand(0, 365));
    }
}
