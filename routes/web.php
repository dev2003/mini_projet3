<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PostController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\InscriptionController;
use App\Http\Controllers\DeconnexionController;


Route::get('/', function () {
    return view('welcome');
});


Route::resource("post", PostController::class);


Route::middleware("guest")->group(function(){
    // Les routes d'inscription
    Route::get("/register", [InscriptionController::class, "show"]);
    Route::post("/register", [InscriptionController::class, "register"])->name("register");


    Route::get('/login',[AuthController::class,'show'])->name('login.show');
    Route::post('/login',[AuthController::class,'login'])->name('login');
});


Route::middleware("auth")->group(function(){
    Route::post("/logout", [DeconnexionController::class, "logout"])
        ->name("logout");
});
